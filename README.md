# Usage
To print out report of holidays for all subordinates of given person:

    python  bamboohr-holidays.py <bamboohr-api-key> <display-name> <subdomain>
    
    
For instance, to display reports of `John Doe` in subdomain `test` with api key `abc123`:

    python  bamboohr-holidays.py abc123 "Doe, John" test
 

Output will contain holidays longer than 2 days for current calendar year