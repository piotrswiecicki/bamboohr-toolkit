import datetime
import sys
from datetime import date
from datetime import timedelta

import requests

HOLIDAYS_ID = '1'
API_KEY = sys.argv[1]
MANAGER_ID = sys.argv[2]
SUBDOMAIN = sys.argv[3]

def parse_date(datestr):
    return datetime.datetime.strptime(datestr, "%Y-%m-%d")


def get_employees(manager):
    r = requests.get("https://api.bamboohr.com/api/gateway.php/"+SUBDOMAIN+"/v1/employees/directory/",
                     headers={'Accept': 'application/json'},
                     auth=(API_KEY, 'x'))
    return [(person["id"], person["displayName"]) for person in r.json()["employees"] if
            person["supervisor"] == manager]


for (id, name) in get_employees(MANAGER_ID):
    r = requests.get("https://api.bamboohr.com/api/gateway.php/"+SUBDOMAIN+"/v1/time_off/requests/",
                     params={"employeeId": id, "type": HOLIDAYS_ID, "status": "approved",
                             "start": date(date.today().year, 1, 1),
                             "end": date(date.today().year, 12, 31)},
                     headers={'Accept': 'application/json'},
                     auth=(API_KEY, 'x'))

    print(name)
    for leave in r.json():
        end = parse_date(leave["end"])
        start = parse_date(leave["start"])
        duration = end - start + timedelta(days=1)  # +1 day because bamboohr gives inclusive range

        if duration.days > 2:  # skipping obvious cases
            print("{} - {} => {} days".format(leave["start"], leave["end"], duration.days))
